# SFTP-Server

An Ansible role which configures a single tenant OpenSSH server for chrooted SFTP access.

## Requirements

Ansible >= 2.5.2

## Role Variables

The following role variables are relevant:

* `sftp_home_partition`: The partition where SFTP users' home directories will be located.  Defaults to "/home".
* `sftp_group_name`: The name of the Unix group to which all SFTP users must belong.  Defaults to "sftpusers".
* `sftp_directories`: A list of directories that need to be created automatically by default for all SFTP user. Defaults to a blank list (i.e. "[]").
  * Values can be plain strings, or dictionaries containing `name` and (optionally) `mode` key/value pairs.
* `sftp_allow_passwords`: Whether or not to allow password authentication for SFTP. Defaults to False.
* `sftp_enable_selinux_support`: Whether or not to explicitly enable SELinux support. Defaults to False.
* `sftp_enable_logging`: Enable logging. Auth logs will be written to `/var/log/sftp/auth.log`, and SFTP activity logs will be written to `/var/log/sftp/verbose.log`. Defaults to False.
* `sftp_users`: A list of users, in map form, containing the following elements:
  * `name`: The Unix name of the user that requires SFTP access.
  * `group`: An optional user primary group. If set, it will be used for the user's home permission. Otherwise, the `sftp_group_name` is used.
  * `password`: A password hash for the user to login with.  Blank passwords can be set with `password: ""`.  NOTE: It appears that `UsePAM yes` and `PermitEmptyPassword yes` need to be set in `sshd_config` in order for blank passwords to work properly.  Making those changes currently falls outside the scope of this role and will need to be done externally.
  * `shell`: Boolean indicating if the user should have a shell access (default to `True`).
  * `public_key_material`: An optional list of files placed in `files/` which contain valid public keys for the SFTP user.
  * `sftp_directories`: A list of directories that need to be individually created for an SFTP user. Defaults to a blank list (i.e. "[]").
  * `append`: Boolean to add `sftp_group_name` to the user groups (if any) instead of setting it (default to `False`).
  * `mode`: The users home directory mode (defaults to `0750`).
  * `skeleton`: An optional home skeleton directory (e.g: /dev/null). Default to system defaults.
* `sftp_nologin_shell`: The "nologin" user shell. (defaults to /usr/sbin/nologin.)


## Example Playbook

```yaml
---
  - hosts: localhost
    roles:
      - role: ansible-sftp-server
        sftp_home_partition: /opt/sftp
        sftp_enable_logging: true
        sftp_users:
          - name: skinny
            public_key_material: ['ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDrR4Im4sVAA98BWEs8kZrEkhzGsggmGFozbVhmAn7sfFxKpRopAwVfvS01zcyNbpm0D369329CWdDL/L8ufs8O3aMXYxY8iS8OZX7akaKqrRWrI7JDWlJ103Fe8Ls440PgdSQP46yTFMd0sIyd2LTCuqrhsoB05bCZFyX//95ECMno6U3Jdltoc3WwHLRAe0GTBi4zMKP8ihuVQt/h0a7M9/BhKVDgJqbStMlEHpXSyvZ8l3rILK0tfA4qy6gN2ltV80zt8F9/LNquoB0reBA4LxKSPPaMYyiJhocnvFbbSLe1/YCxQeoUcmsfT4jI/Q+6f0jOHV5hb+zPdYAaK39S5ZI1WhxF0hUGWQcn9L6E2AMotaBAP917LAVCl7dLEGy5B2mNjxdpGq9DKpbEWy/ZO5IUc8VwCsRbM9OckvQrd7jq3F7odAr0LlRoaAtmz4gDgkJ84M2n4QuTWm2iV5j1K4rdDHW5zb/zL0DTaAKcjwKBymznac1WHmj1GZdHi9cvMPK3te9idqnoE4Gu2i+9TpmnycB6NJBue3JpqXaa6uZiS8GtwdDRaRNr5BdEKaMtVQnsw2Kf/PSRSHmbt9tejTR3YcHUZhjXxRivC0Cthk/10LfAi38J8ztxMLRCiD4WVE+HbslONgx7Jvk7EdCoSYVmUq4u4IglCsOk8m4FCQ== christiancosta@eds30880330.local']
            shell: False
            sftp_directories:
              - crce
              - crm
              - xdr
```
